# -*- coding: utf-8 -*-
# ---
# jupyter:
#   celltoolbar: Slideshow
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.3
#   kernelspec:
#     display_name: Bash
#     language: bash
#     name: bash
#   language_info:
#     codemirror_mode: shell
#     file_extension: .sh
#     mimetype: text/x-sh
#     name: bash
#   latex_envs:
#     LaTeX_envs_menu_present: true
#     autoclose: false
#     autocomplete: true
#     bibliofile: biblio.bib
#     cite_by: apalike
#     current_citInitial: 1
#     eqLabelWithNumbers: true
#     eqNumInitial: 1
#     hotkeys:
#       equation: Ctrl-E
#       itemize: Ctrl-I
#     labels_anchors: false
#     latex_user_defs: false
#     report_style_numbering: false
#     user_envs_cfg: false
#   nbpresent:
#     slides:
#       12b9fe3a-a338-4c02-a1a5-7361ffe278a0:
#         id: 12b9fe3a-a338-4c02-a1a5-7361ffe278a0
#         prev: 6ff85066-163e-45a8-8d6b-4c0300d380c8
#         regions:
#           f4b64b97-9426-4ddf-bbf8-ca58eb3ec9c6:
#             attrs:
#               height: 0.8
#               width: 0.8
#               x: 0.1
#               y: 0.1
#             content:
#               cell: 4a3b0875-2707-4c12-8f73-77dfacbb676b
#               part: whole
#             id: f4b64b97-9426-4ddf-bbf8-ca58eb3ec9c6
#       6ff85066-163e-45a8-8d6b-4c0300d380c8:
#         id: 6ff85066-163e-45a8-8d6b-4c0300d380c8
#         prev: null
#         regions:
#           fac442f3-65b1-4c53-9ad4-6e6aea20397e:
#             attrs:
#               height: 0.8
#               width: 0.8
#               x: 0.1
#               y: 0.1
#             content:
#               cell: 37561893-cd2c-4670-bf38-742e4b1d07b8
#               part: whole
#             id: fac442f3-65b1-4c53-9ad4-6e6aea20397e
#       dc2f215a-a42b-47a6-b998-abb47edc18df:
#         id: dc2f215a-a42b-47a6-b998-abb47edc18df
#         prev: 12b9fe3a-a338-4c02-a1a5-7361ffe278a0
#         regions:
#           7992c4bd-af7d-49d3-bdf4-32435093d8a5:
#             attrs:
#               height: 0.4
#               width: 0.8
#               x: 0.1
#               y: 0.5
#             content:
#               cell: a66d9504-7117-4417-bad3-288fbf8f0360
#               part: whole
#             id: 7992c4bd-af7d-49d3-bdf4-32435093d8a5
#           e2679959-51d4-41f5-a44e-e22cd26ca5ca:
#             attrs:
#               height: 0.4
#               width: 0.8
#               x: 0.1
#               y: 0.5
#             content:
#               cell: bc15053c-ba71-4cab-b8fd-dff7bc3f55e8
#               part: whole
#             id: e2679959-51d4-41f5-a44e-e22cd26ca5ca
#           e39392fa-e59c-4d93-9cae-9a5d7b774f2e:
#             attrs:
#               height: 0.8
#               width: 0.8
#               x: 0.1
#               y: 0.1
#             content:
#               cell: 3ed12352-48a1-4530-8e6e-d438a08c8239
#               part: whole
#             id: e39392fa-e59c-4d93-9cae-9a5d7b774f2e
#     themes: {}
# ---

# + {"nbpresent": {"id": "37561893-cd2c-4670-bf38-742e4b1d07b8"}, "slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# <h1><center>How to blame your genes with confidence</center></h1>
# <h3><center><a href="http://einon.net/blame-genes-confidence">einon.net/blame-genes-confidence</a></center></h3>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics &amp; Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>
# <h3><center>Gitlab.com: einonm</center></h3>
# <h3><center>Twitter: @einonm</center></h3>

# + {"active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
# </script>

# + {"nbpresent": {"id": "4a3b0875-2707-4c12-8f73-77dfacbb676b"}, "slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Some background...
#
# * I'm the HPC Systems Manager at the MRC CNGG, part of Cardiff University and also a part time PhD student.
# * The Centre is involved in research into the genetics of common brain related disorders – Alzheimer's, Parkinson's, Schizophrenia, Bipolar, ADHD...
# * Part of this research involves taking large numbers of individual's genomes and performing statistical analysis
# * Large HPC clusters are used for this, running entirely free and open source software, on Linux based OS's

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## What is DNA?
#
# **Everyone has DNA**
#
# DNA - DeoxyriboNeucleic Acid - is found in the nucleus of all human cells, and is a long double helix strand of molecules called *nucleotides*.
#
# * There are only four types of nucleotide *bases*, and they are always paired within DNA
# * The four types are Adenine, Thyamine, Guanine, Cytosine; referred to by the first letter of each - A, C, T and G
# * A is always paired with T and G is always paired with C
# * In humans, two matching DNA double-helix strands are wrapped together - one from each parent, into a *chromosome*
# * Humans have 23 chromosomes, consisting of 3 billion base pairs making up the *human genome*

# + {"nbpresent": {"id": "3ed12352-48a1-4530-8e6e-d438a08c8239"}, "slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## DNA Helix
#
# ![title](dna-helix.jpg)

# + {"nbpresent": {"id": "bc15053c-ba71-4cab-b8fd-dff7bc3f55e8"}, "slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Human Chromosomes
#
# ![title](chromosomes.gif)
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## What is a gene?
#
# #### * A gene is a section of DNA that creates a protein *
#
# ### How?
#
# * The DNA is 'read', or *transcribed*, to produce a template (RNA) that can create copies of the protein.
# * Only some sections of DNA can produce a protein
# * These sections start with one particular set of three base pairs (ATG) and stop with a few other sets of three bps.
# * A set of three base pairs is called a *codon*. Transcription (and later, translation) mechanisms work on sets of three base pairs at a time.
# * The section of DNA between the start codon and stop codon is a gene.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Transcription
# <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/5MfSYnItYvg?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Translating RNA to protein
#
# * RNA moves out of the cell nucleus, and into the cell where it can be translated to a protein
# * This translation happens one codon at a time, with each codon being translated to one amino acid
# * The protein then interacts with other proteins to do 'things'; a set of proteins that interact together is known as a *pathway*

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Translation
# <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/8dsTvBaUMvw?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Interlude - gene names
#
# ### Previously, the discoverer of a gene had the honour of naming it...
#
# * **Stranded At Second**: A fruit fly that dies, usually in the second larval stage of development
#
# * **Agoraphobic**: A fruit fly with larvae that look normal but never crawl out of the egg shell
#
# * **Cheap Date**: A fruit fly that expresses high sensitivity to alcohol
#
# * **Out Cold**: A fruit fly that loses coordination when the temperature drops
#
# * **Kenny**: A fruit fly without this gene dies in two days, named for the South Park character who dies in each episode
#
# * **Ken and Barbie**: Fruit flies that fail to develop external genitalia
#
# * **I'm Not Dead Yet (INDY)**: These fruit flies live longer than usual. From Monty Python's *The Holy Grail*, "Bring out your dead", "I'm not dead yet!"

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## How unique is *my* DNA?
#
# ** DNA is 99.9% identical for all humans **
#
# * There are small naturally occurring differences in genes, usually only by a single base pair
#
# * E.g. 70% of people have TT in a certain base pair position, others a GT - these are known as *alleles*
#
# * Remeber that we (usually) have two copies of each base pair. If both copies are the same, we say that the allele is *homozygous*. If different, they are *heterozygous*
#
# * These small differences are called Single Nucleotide Polymorphisms, or SNPs (pronounced 'snips')
#
# * SNPs occurring in coding regions of DNA (genes) are of the most interest, but SNPs in non-coding regions can also have interesting effects
#
# * There are more drastic variations such as duplicated or deleted sections of DNA (Called *CNVs* - Copy Number Variations), these can cause major problems – e.g. Prader-Willi syndrome, 22q11 syndrome...

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Gene differences
#
# * We consider SNPs that vary in the population, where the minor allele has a frequency greater than a threshold (e.g. 1%)
#
# * These naturally occurring differences, or combinations of differences, could be the basis of a disorder
#
# * This follows from the fact that varying SNPs in genes cause varying proteins to be created for the gene in the population
#
# * ...and hence these proteins act differently within it's pathways, causing undesired subtle effects
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## How are gene differences linked to disease?
#
# * Genetic causes of some disorders are easy to find, being associated with only one gene (e.g. Huntington's)
# * Most complex disorders are a little more tricky, not only with SNPs in *coding* regions of genes contributing to the disease, but many in *non-coding* regions too.
# * To identify which SNPs are associated with a disease, we perform a ***Case-Control Genome Wide Association Study*** (GWAS)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## GWAS
#
# * Genetically sequence the SNPs (naturally occurring genetic variations) of a large group of individuals with a disorder (cases), and of a large group without (controls)
# * Perform various QC actions to clean the data, and to remove any known bias effects (for example, linkage disequilibrium (LD) and population stratification)
# * Then perform a statstical test for each SNP to see if certain alleles are more associated with the cases than the controls

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## GWAS example
#
# #### Study: GWAS of Parkinsons' Disease (PD)
#
# * Performed on around 6,000 cases and 10,000 controls
# * Produces a *manhattan plot* - each dot is a SNP. x axis - bp, y axis - negative log p value
# * A low p value indicates a big association (higher on the graph is better)
# * Generated mainly using the open source *plink* tool and R scripts.
# <br><br>
#
# Original paper: <a href='http://dx.doi.org/10.1016/S0140-6736(10)62345-8'>http://dx.doi.org/10.1016/S0140-6736(10)62345-8</a>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## PD GWAS results
# ![title](pd-manhattan.png)
