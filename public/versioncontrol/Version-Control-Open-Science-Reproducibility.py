# -*- coding: utf-8 -*-
# ---
# jupyter:
#   celltoolbar: Slideshow
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 3
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython3
#     version: 3.7.1
#   latex_envs:
#     LaTeX_envs_menu_present: true
#     autoclose: false
#     autocomplete: true
#     bibliofile: biblio.bib
#     cite_by: apalike
#     current_citInitial: 1
#     eqLabelWithNumbers: true
#     eqNumInitial: 1
#     hotkeys:
#       equation: Ctrl-E
#       itemize: Ctrl-I
#     labels_anchors: false
#     latex_user_defs: false
#     report_style_numbering: false
#     user_envs_cfg: false
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# <h1><center>Bioinformatics Workshop:<center></h1>
# <h1><center>Version Control, Open Science &amp; Reproducibility</center></h1>
# <h3><center><a href="http://einon.net/versioncontrol">einon.net/versioncontrol</a></center></h3>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics &amp; Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>
# <h3><center>Gitlab.com: einonm</center></h3>
# <h3><center>Twitter: @einonm</center></h3>

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#
# </script>
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# #  Reproducibility, Open Science &amp; Version Control

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# ## What is reproducibility?

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# ## What is replicability?

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What is reproducibility?
#
#

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# * The ability of an entire experiment to be duplicated by the same, or a different researcher, to obtain the same results _exactly_ using the same code and data.
# * Concentrates on copying results.
# * For _in-silico_ experiments, given the same data and code, this should be a no-brainer?

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What is replicability?

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# * According to the dictionary, pretty much the same as reproducibility!
# * A (rather vague) difference between the meaning of replicability and reproducibility exists in scientific literature.
# * The concept of replicability seems to be to repeat _findings_. Statistical power, bias, experiment design and results interpretation are also taken into account.
# * Assumes that independent data & methods are used.
# * Concentrates on validating results.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # The difference is not widely used (yet!)...
#
# ### https://en.wikipedia.org/wiki/Replication_crisis
#
# ![title](img/wikipediaReplic.png)
#

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# ![title](img/wikipediaReproduc.png)
#
# <br>
# <small>1. Leek, Jeffrey T; Peng, Roger D. "Reproducible research can still be wrong: Adopting a prevention approach". Proceedings of the National Academy of Sciences of the United States of America. 112 (6): 1645–1646. doi 10.1073/pnas.1421412111 </small>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
#
# ![title](img/ManifestoReproSci.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # So let's define...
#
# * **Reproducibility as the ability to independently recompute results verbatim, using the same procedure and data**
#
# * **Replicability as the ability to achieve consistent results, using independent procedures and different data**
#
#
#
# <br><br>
#
# <small>1. http://languagelog.ldc.upenn.edu/nll/?p=21956 (Replicability vs. reproducibility — or is it the other way around?)</small>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Using these definitions...
#
# * Reproducibility is an easier problem to solve than replicability - it can potentially be done by one individual
# * ...especially for _in-silico_ experiments, where well tested tools and methods are used in many non-academic areas to achieve this.
#
# ### But...
# * "...incentives for individual scientists prioritize novelty over replication" [1]
# * Reproduction still needs to be incentivised! 
#
#
# <br>
#
# #### _Open Science_ is one way that has been mooted to solve the 'reproducibility/replicability crisis' [2].
#
#
# <br><br>
# <small>1. Collaboration, O.S. (2015). Estimating the reproducibility of psychological science. Science 349:aac4716.</small>
# <small>2. Munafò, M. 2016a. Open Science and Research Reproducibility. ecancermedicalscience 10, p. . doi: 10.3332/ecancer.2016.ed56.</small>
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # What is Open Science?

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# * Open access 
# * Open data
# * Open source code 
#
# <br>
#
# * **Manifesto for reproducible science:**
# "..open science refers to the process of making the content and process of producing evidence and claims transparent and accessible to others...the 'open' should be redundant, but it is not."

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # What is Open Science?
#
# * Open access:  controlled by journals / funders (RCUK [1])
# * Open data:  controlled by funders / consent agreements & ethics
# * Open source code:  controlled by researchers / funders (MRC).
#
# <br><br>
# <small>1. http://www.rcuk.ac.uk/research/openaccess/policy/</small>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # MRC guidelines on Open Source Software (OSS)
#
# * "...applicants whose proposed research aims to produce software outputs must specify a proposed software exploitation route in the case for support. When the project is completed, the software should be exploited either commercially, within an academic community or as OSS" [1].
#
# <br>
#
# (N.B. Why not both OSS and commercial exploitation?)
#
# <br><br>
# <small>1. https://www.mrc.ac.uk/funding/guidance-for-applicants/5-ethics-and-approvals/</small>

# + {"slideshow": {"slide_type": "notes"}, "cell_type": "markdown"}
# Adopt an open by default policy - justify keeping it closed, not other way around.
#
# # Are current Open Science policies facilitating reproducibility?

# + {"slideshow": {"slide_type": "notes"}, "cell_type": "markdown"}
# Reading Uni guide very good
# Software Sustainability Institute
# pfern.github.io/OSODOS/gitbook - open science, open documentation, open source

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Open licensing code, data and publications
#
# **(FYI, IANAL...)** 
#
# In order to share, a license is needed...
#
# * **Why would you need to apply a license?**
#      * To comply with open science policies, letting users know how they can use, distribute and extend the work

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# * **What happens if no license is given with a piece of work, or I have just a copyright statement?**

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# *   * "However, without a license, the default copyright laws apply, meaning that you retain all rights to your source code and no one may reproduce, distribute, or create derivative works from your work." [1]
#     
#     
# <small>1. https://help.github.com/articles/licensing-a-repository/</small>

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# * **What if I want a DOI for my code?**
#     * Check out https://Zenodo.org from CERN, and get a DOI for it. 

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Any thoughts?
#
# ![title](img/magma.png)
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Broad categories of open licenses:
#
# * **Copyleft (e.g. GPL)**
#     * Allows sharing of source code, governed by the 'four freedoms' - to run the code, to study it's source, to redistribute the code and to redistribute modifications. Implies that if a compiled program is shared, one must share the source code too. 'Gold standard' when it comes to ethical licensing.
#
# * **Permissive (e.g. MIT)**
#     * What you'd first imagine an open source license would look like - allows you to do anything with the code, including incorporating it into a piece of proprietary software as there's no need to share code. 
#     
#     Is this more 'free' than a copyleft license? Gives you more freedom, but restricts the freedom of those getting the compiled program, as they are not entitled to the source. 
#
#     (N.B. Permissive is the one to go for if you may intend to spin off a company to commercially exploit the code at a later date!) 
#
# * **Creative Commons (e.g. CC-by-SA)**
#     * A suite of licenses designed for non-compiled works - text, publications and data.

# + {"slideshow": {"slide_type": "notes"}, "cell_type": "markdown"}
# Sometimes attribution, non-commercial clauses of some licenses to be considered.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # How would you record an in-silico procedure?

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Jupyter notebooks
#
# A useful tool to facilitate reproducible, open sharing of research is the jupyter notebook.
#
# "...an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and explanatory text"
#
# There's a more detailed set of slides available at: http://einon.net/jupyternb-arcca-talk/

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Why should you use version control?

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Version control
#
# * From Software Carpentry:
# A tool for managing changes to a set of files. Each set of changes creates a new commit of the files; the version control system allows users to recover old commits reliably, and helps manage conflicting changes made by different users.
#
# https://swcarpentry.github.io/git-novice/01-basics/
#
#
#
# ![xkcd.com](https://imgs.xkcd.com/comics/git.png)
# <center>xkcd.com</center>
#
#
# -


