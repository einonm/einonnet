# -*- coding: utf-8 -*-
# ---
# jupyter:
#   anaconda-cloud: {}
#   celltoolbar: Slideshow
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.3
#   kernelspec:
#     display_name: Bash
#     language: bash
#     name: bash
#   language_info:
#     codemirror_mode: shell
#     file_extension: .sh
#     mimetype: text/x-sh
#     name: bash
#   latex_envs:
#     LaTeX_envs_menu_present: true
#     autoclose: false
#     autocomplete: true
#     bibliofile: biblio.bib
#     cite_by: apalike
#     current_citInitial: 1
#     eqLabelWithNumbers: true
#     eqNumInitial: 1
#     hotkeys:
#       equation: Ctrl-E
#       itemize: Ctrl-I
#     labels_anchors: false
#     latex_user_defs: false
#     report_style_numbering: false
#     user_envs_cfg: false
# ---

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# <h1><center>What is the Jupyter Notebook?</center></h1>
# <h3><center><a href="http://einon.net/jupyternb-arcca-talk">einon.net/jupyternb-arcca-talk</a></center></h3>
# <h3><center>Mark Einon, HPC Systems Manager</center></h3>
# <h3><center>MRC Centre for Neuropsychiatric Genetics &amp; Genomics</center></h3>
# <h3><center>einonm@cardiff.ac.uk</center></h3>
# <h3><center>Gitlab.com: einonm</center></h3>
# <h3><center>Twitter: @einonm</center></h3>

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # What is the Jupyter Notebook? 

# + {"slideshow": {"slide_type": "-"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#
# </script>
#

# + {"slideshow": {"slide_type": "-"}, "cell_type": "markdown"}
#
# ### Talk aims:
#
# * Introduce the Jupyter Notebook to the uninitiated
# * Show how to run a notebook on local clusters
# * Breifly cover options for other methods for running on clusters
# ![title](img/jupyter.png)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # What is the Jupyter Notebook?

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# A tool for reproducible science in the form of a souped-up REPL (Read–Eval–Print Loop) environment.
#
# Or, from [jupyter.org](http://jupyter.org):<br>
#
# "...an open-source web application that allows you to create and share documents that contain live code, equations, visualizations and explanatory text"<br>
#
# "...these documents provide a complete and self-contained record of a computation that can be converted to various formats and shared with others using email, Dropbox, version control systems (like git/GitHub) or nbviewer.jupyter.org"

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ![title](img/jupyter-used-at.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## What's that about code?
#
# Jupyter has support for over 40 languages, including Python 2/3, R, Bash, C, C++, Julia, Scala, Go, Fortran, Perl...
#
# We only use 3 at present - Bash, Python and R.
#
# These are run as interactive processes, called **jupyter kernels**, one kernel runs per notebook.

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# This provides a very fast feedback loop for code development and exploratory data analysis (EDA).
#
# Notebooks can be saved as native executable code files (.py, .sh, .c ...etc) as well as html, pdf, LaTeX & markdown.

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Example use cases for Jupyter Notebooks:
# ### [A 'lab book' for code](Parse_eQTL_data.html)
#
# ### [exploratory data analysis](Parse_eQTL_data.html)
#
# ### [defining analysis pipelines](Parse_eQTL_data.html)
#
# ### [Teaching notes](Parse_eQTL_data.html)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## Example use cases for Jupyter Notebooks:
# ### [Interactive exercises](http://einon.net/jupyternb-arcca-talk/hello_world.ipynb)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## Example use cases for Jupyter Notebooks:
#
# ### Auto grading workbooks
#
# ![title](img/creating_assignment.gif)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## Example use cases for Jupyter Notebooks:
#
# Fancy slides
#
# (hit Esc / Ctrl-Tab)
#
# Notebooks can be rendered into a Reveal.js slideshow, and benefit from all it's layout options.
#
# (E.g. add ?transition=[convex,concave,zoom,cube...] to the URL)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Running the Jupyter Notebook on a local cluster
#
# A Jupyter Notebook is not much use for **big data** if it can only be run on a laptop - need to be able to run it on a cluster with more appropriate resources.
#
# For the genomic datasets used in the CNGG, this is certainly the case, if Jupyter Notebooks are to be useful.
#
# The 'hacky way' to run notebooks on a cluster described below currently works.
#
# The 'official' IPython parallel way is not yet implemented fully on the ARCCA or CNGG local clusters.

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # Running on a cluster - the 'hacky' way
#
# It's possible to run a personal stand-alone jupyter notebook instance on the Rocks (CNGG) or Raven (ARCCA) cluster, with the help of a few scripts, available from github at [https://gitlab.com/einonm/jupyternb-setup](https://gitlab.com/einonm/jupyternb-setup):
#
# ![title](img/cluster-hack.png)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## The hacky way - step 1
#
# The user logs onto the cluster head node

# + {"slideshow": {"slide_type": "-"}}
ssh raven

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## The hacky way - step 2
#
# The user starts an interactive job shell on the cluster with the relevant resources

# + {"slideshow": {"slide_type": "-"}}
qsub -I -P <user-project-id> -q <queue>

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## The hacky way - step 3
#
# The user runs the notebook server within the interactive job shell.
#
# With the latest versions of jupyter, 4.3+ (Available on Rocks, not Raven yet), there is a token to use, and the below is also displayed. Use this token URL to access the server later.
#     
#     Copy/paste this URL into your browser when you connect for the first time,
#     to login with a token:
#         http://localhost:8888/?token=013ceabdd6dca7ef60d04db2ac5103eb70b0da5603d9ceb1

# + {"slideshow": {"slide_type": "-"}}
./run_jupyter_notebook_server.sh

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## The hacky way - step 4
#
# As a cluster node can only be seen from the login node, then a browser has to be started there to use notebooks running on the cluster.
#
# So the user logs onto the cluster with another, X-based, session.
#
# ...and then runs <tt>firefox</tt> from this session.

# + {"slideshow": {"slide_type": "-"}}
ssh -X raven
firefox

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# ## The hacky way - step 5
#
# In the browser address bar, the user can enter the cluster node name and port given above, i.e.:
#
#  http://raven92:8891
#  
#  
#  ![title](img/raven-firefox.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Running on a cluster - ipyparallel
#
# ipyparallel is a Python package and collection of CLI scripts for controlling clusters for Jupyter.
#
# ipyparallel contains the following CLI scripts:
#
# * **ipcluster** - start/stop a cluster
# * **ipcontroller** - start a scheduler
# * **ipengine** - start an engine
#
# ![title](img/ipyparallel-screen.png)

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # ipyparallel scripts
#
# * ipcluster configures a default profile which creates one controller and can use all cores on localhost, starting an engine for each.
#
# * ip cluster can also be configured to listen for other engines available across the cluster network (started via PBS/SGE).
#
# * One engine is the equivalent to one jupyter kernel as used previously, with the exception that an engine is connected to a controller, not a web frontend.
#
# * Controllers and engines can be confgured as needed, with the configurations stored as *profiles*.
#
# * It's envisaged that separate profiles are created for each use case.
#
#

# + {"slideshow": {"slide_type": "subslide"}, "cell_type": "markdown"}
# # JupyterHub
#
# As well as a stand alone server, a multi-user *JupyterHub server* can be installed
# (The CNGG have a VM setup with this for noodling with).
#
# Three main actors make up JupyterHub:
#
# * multi-user Hub (tornado process)
# * configurable http proxy (node-http-proxy)
# * multiple single-user Jupyter notebook servers (Python/IPython/tornado)
#
# Basic principles for operation are:
#
# * Hub spawns a proxy.
# * Proxy forwards all requests to Hub by default.
# * Hub handles login, and spawns single-user servers on demand.
# * Hub configures proxy to forward url prefixes to the single-user notebook servers.
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Links
#
# * [jupyter](https://jupyter.org)
# * [jupyterhub](https://jupyterhub.readthedocs.io/en/latest)
# * [ipyparallel](https://ipyparallel.readthedocs.io/en/latest/)
# * [Reveal.js](http://lab.hakim.se/reveal-js/#)
# <br><br>
# * [GitHub repo of jupyterNB setup scripts](https://gitlab.com/einonm/jupyternb-setup)
# * [GitHub repo of these slides](https://gitlab.com/einonm/jupyternb-arcca-talk)
# -


