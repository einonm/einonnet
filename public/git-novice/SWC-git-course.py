# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.3
#   kernelspec:
#     display_name: Bash
#     language: bash
#     name: bash
# ---

# + {"slideshow": {"slide_type": "notes"}, "active": ""}
# <script>
#   $( document ).ready(function(){
#     <!--$('div.input').hide()-->
#     $('div.prompt').hide()
#   });
#
# </script>

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Version control with git
#
# http://swcarpentry.github.io/git-novice/
#
# http://einon.net/git-novice

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Automated version control - much better than:
# ![title](phd-com-small.png)
# “Piled Higher and Deeper” by Jorge Cham, http://www.phdcomics.com

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Making changes
# ![title](play-changes.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Independent changes
# ![title](versions.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Merging changes
# ![title](merge.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Setting up git
#
# Tell git our name and email:

# + {"slideshow": {"slide_type": "fragment"}}
git config --global user.name "Vlad Dracula"
git config --global user.email "vlad@tran.sylvan.ia"

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Set a preferred text editor:

# + {"slideshow": {"slide_type": "fragment"}}
git config --global core.editor "nano -w"

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Set an alias for a long command:

# + {"slideshow": {"slide_type": "fragment"}}
git config --global alias.tree 'git log --decorate --color --oneline --graph --all'

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Check settings:

# + {"slideshow": {"slide_type": "fragment"}}
git config --list

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Git help:

# + {"slideshow": {"slide_type": "fragment"}}
git config -h

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Git manual:

# + {"slideshow": {"slide_type": "fragment"}}
git config --help

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Creating a repository
# ![title](motivatingexample.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# # Creating a repository
# Create a directory:

# + {"slideshow": {"slide_type": "fragment"}}
cd ~/Desktop
mkdir planets
cd planets

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Make a git repository:

# + {"slideshow": {"slide_type": "fragment"}}
git init

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Find the hidden .git directory:

# + {"slideshow": {"slide_type": "fragment"}}
ls

# + {"slideshow": {"slide_type": "fragment"}}
ls -a

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Check status:

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Tracking changes
# Create a file, entering the text as below:

# + {"slideshow": {"slide_type": "fragment"}}
#nano mars.txt
echo 'Cold and dry, but everything is my favourite colour' > mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Check status:

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Add the changes:

# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git status

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Commit the changes:

# + {"slideshow": {"slide_type": "fragment"}}
git commit -m "Start notes on Mars as a base"

# + {"slideshow": {"slide_type": "slide"}}
git log

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Where are my changes?
# Some more text is added to the file:

# + {"slideshow": {"slide_type": "fragment"}}
# nano mars.txt
echo 'The two moons may be a problem for Wolfman' >> mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Look at the changes:

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "fragment"}}
git diff

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Commit:

# + {"slideshow": {"slide_type": "fragment"}}
git commit -m "Add concerns about effects of Mars' moons on Wolfman"
git status

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Whoops! I didn't 'add' first...

# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git commit -m "Add concerns about effects of Mars' moons on Wolfman"

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## The staging area
# ![title](git-staging-area.svg)
# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Add another line to the file:
# + {"slideshow": {"slide_type": "fragment"}}
# nano mars.txt
echo 'But the Mummy will appreciate the lack of humidity' >> mars.txt
cat mars.txt
# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Examine the differences:


# + {"slideshow": {"slide_type": "fragment"}}
git diff


# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Add the file, look again:


# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git diff


# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Where are the changes?

# + {"slideshow": {"slide_type": "fragment"}}
git diff --staged

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Commit, and check changes:

# + {"slideshow": {"slide_type": "fragment"}}
git commit -m "Discuss concerns about Mars' climate for Mummy"

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "slide"}}
git log

# + {"slideshow": {"slide_type": "fragment"}}
git tree

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Exploring history
#
# Add another line to the file:

# + {"slideshow": {"slide_type": "fragment"}}
# nano mars.txt
echo 'An ill-considered change' >> mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# See what we've done, using HEAD:

# + {"slideshow": {"slide_type": "fragment"}}
git diff HEAD mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# We can use this terminology to look at the previous commit diff:

# + {"slideshow": {"slide_type": "fragment"}}
git diff HEAD~1 mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Or one several commits back:

# + {"slideshow": {"slide_type": "fragment"}}
git diff HEAD~2 mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# We could use the commit ID:

# + {"slideshow": {"slide_type": "fragment"}}
git diff 4b4bb9d171762413b13be6d18dd863a5edb193a4 mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Or a shorter version, first few chars (as long as it's unique):

# + {"slideshow": {"slide_type": "fragment"}}
git diff 4b4bb mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Now git tells us:

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Put things back the way they were:

# + {"slideshow": {"slide_type": "fragment"}}
git checkout HEAD mars.txt

# + {"slideshow": {"slide_type": "fragment"}}
cat mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# We can go back further:

# + {"slideshow": {"slide_type": "fragment"}}
git checkout 4b4bb mars.txt

# + {"slideshow": {"slide_type": "fragment"}}
cat mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Note that changes are staged:

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Then put things back as they were:

# + {"slideshow": {"slide_type": "fragment"}}
git checkout HEAD mars.txt

# + {"slideshow": {"slide_type": "fragment"}}
git status

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Don't lose your HEAD
# Previously we used:
# ````
# $ git checkout 4b4bb mars.txt
# ````
#
# But be careful! omitting the filename does something else:

# + {"slideshow": {"slide_type": "fragment"}}
git checkout 4b4bb

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Revert by checking out the master branch:

# + {"slideshow": {"slide_type": "fragment"}}
git checkout master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Remember, use the commit before the change being undone:
# ![title](git-checkout.svg)
#

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Here's a lovely cartoon of how git works:
# ![title](git_staging.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Ignoring things
#
# Create some dummy data files and see what git says:

# + {"slideshow": {"slide_type": "fragment"}}
mkdir results
touch a.dat b.dat c.dat results/a.out results/b.out
git status

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Let's tell git to ignore them:

# + {"slideshow": {"slide_type": "fragment"}}
# nano .gitignore
echo '*.dat' > .gitignore
echo 'results/' >> .gitignore
cat .gitignore

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Now see what git thinks:
# + {"slideshow": {"slide_type": "fragment"}}
git status
# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Add and commit the new file:
# + {"slideshow": {"slide_type": "fragment"}}
git add .gitignore
git commit -m "Ignore data files and the results folder."
# + {"slideshow": {"slide_type": "fragment"}}
git status
# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# .gitignore also help avoid accidentally adding ignored files:


# + {"slideshow": {"slide_type": "fragment"}}
git add a.dat

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# We can also see the status of ignored files if we want:

# + {"slideshow": {"slide_type": "fragment"}}
git status --ignored

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Remotes in GitLab

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Go to https://gitlab.psycm.cf.ac.uk, and select 'New project':
# ![title](gitlab1.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Enter 'planets' for the project name, make it a public repository and click 'Create project':
# ![title](gitlab2.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# GitLab then displays some info on how to configure your local repository:
# ![title](gitlab3.png)

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# This effectively does the following on the GitLab server:
# ````
# $ mkdir planets
# $ cd planets
# $ git init
# ````

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Remembering from earlier, our local repository looks like:
# ![title](git-staging-area.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Now that there are two repositories, things look like:
# ![title](git-freshly-made-github-repo.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Connect the two repos, first find the remote (HTTPS) address:
# ![title](gitlab4.png)

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Copy the URL, and in our local repository add the remote and check:

# + {"slideshow": {"slide_type": "fragment"}}
git remote add origin https://Tester@gitlab.psycm.cf.ac.uk/Tester/planets.git
git remote -v

# + {"slideshow": {"slide_type": "slide"}}
git push origin master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Collaborating
# Find a partner (your Collaborator), and give them write access to your GitLab repository (they should also receive a comfirmatory email):
# ![title](gitlab5.png)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# The Collaborator should download the repository using the URL supplied on the project page:

# + {"slideshow": {"slide_type": "fragment"}}
git clone https://gitlab.psycm.cf.ac.uk/Tester/planets.git ~/Tester-planets

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ![title](github-collaboration.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# The Collaborator can now make a change:

# + {"slideshow": {"slide_type": "fragment"}}
cd ~/Tester-planets
#nano pluto.txt
echo 'It is so a planet!' > pluto.txt
cat pluto.txt

# + {"slideshow": {"slide_type": "fragment"}}
git add pluto.txt
git commit -m "Add notes about Pluto"

# + {"slideshow": {"slide_type": "slide"}}
git tree

# + {"slideshow": {"slide_type": "fragment"}}
git push origin master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# As the owner, your remote is now different from your local repo. So let's download the changes:

# + {"slideshow": {"slide_type": "fragment"}}
#cd ~/Desktop/planets
git pull origin master

# + {"slideshow": {"slide_type": "fragment"}}
git tree

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ## Conflicts
# To resolve a conflict, we must first create one. Let's add a line to one partner's copy only:

# + {"slideshow": {"slide_type": "fragment"}}
#nano mars.txt
echo "This line added to Wolfman's copy" >> mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "-"}}
git tree

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# then push the changes to GitLab:

# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git commit -m "Add a line in our home copy"

# + {"slideshow": {"slide_type": "fragment"}}
git push origin master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Now let's make the other partner update their local copy differently *without* updating:

# + {"slideshow": {"slide_type": "fragment"}}
#cd ~/Tester-planets/
#nano mars.txt
echo "We added a different line in the other copy" >> mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# And commit this change locally:

# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git commit -m "Add a line in my copy"

# + {"slideshow": {"slide_type": "slide"}}
git tree

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# But git won't let us push this to GitLab!

# + {"slideshow": {"slide_type": "fragment"}}
git push origin master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# ![title](conflict.svg)

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# What we have to do is pull the changes from GitLab and merge. Start by pulling:

# + {"slideshow": {"slide_type": "fragment"}}
git pull origin master

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# 'git pull' has attempted to merge automatically, and failed - but marked the conflict: 

# + {"slideshow": {"slide_type": "fragment"}}
cat mars.txt

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Lets replace both lines, so the file looks like this:

# + {"slideshow": {"slide_type": "fragment"}}
# nano mars.txt
cat mars.txt

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# To finish the merge, add the changes:

# + {"slideshow": {"slide_type": "fragment"}}
git add mars.txt
git status

# + {"slideshow": {"slide_type": "fragment"}}
git commit -m "Merge changes from GitLab"

# + {"slideshow": {"slide_type": "slide"}, "cell_type": "markdown"}
# Then push back to GitLab:

# + {"slideshow": {"slide_type": "fragment"}}
git push origin master

# + {"slideshow": {"slide_type": "slide"}}
git tree

# + {"slideshow": {"slide_type": "fragment"}, "cell_type": "markdown"}
# Then the Collaborator pulls the fixed changes:

# + {"slideshow": {"slide_type": "fragment"}}
#cd ~/Desktop/planets
git pull origin master
